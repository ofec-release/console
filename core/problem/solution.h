/******************************************************************************
* Project:Open Frameworks for Evolutionary Computation (OFEC)
*******************************************************************************
* Author: Changhe Li
* Email: changhe.lw@gmail.com
* Language: C++
*-------------------------------------------------------------------------------
*  This file is part of OFEC. This library is free software;
*  you can redistribute it and/or modify it under the terms of the
*  GNU General Public License as published by the Free Software
*  Foundation; either version 2, or (at your option) any later version.
*
*  see https://github.com/Changhe160/OFEC for more information
*
*-------------------------------------------------------------------------------
* class solution is designed for any kind of user-defined solution representation.
* It takes a variable encoding and an objective value type as parameters to generate
* a solution.
*
*********************************************************************************/

#ifndef OFEC_SOLUTION_H
#define OFEC_SOLUTION_H

#include <utility>
#include "../instance_manager.h"

namespace  ofec {

	template<typename TVar = VarVec<Real>>
	class Solution : public SolBase {
	public:
		using VarEnc = TVar;
		virtual ~Solution() {}
		Solution() = default;
		template<typename ... Args>
		Solution(size_t num_objs, size_t num_cons, Args&& ... args) :
			SolBase(num_objs, num_cons), m_var(std::forward<Args>(args)...) {}
		Solution(const Solution &rhs) = default;
		Solution(Solution &&rhs) = default;
		Solution(const SolBase& rhs) : Solution(dynamic_cast<const Solution&>(rhs)) {}
		Solution(SolBase&& rhs) : Solution(dynamic_cast<Solution&&>(rhs)) {}

		Solution &operator =(const Solution &rhs) = default;
		Solution &operator =(Solution &&rhs) = default;
		const Solution& solut() const noexcept { return *this; }
		Solution& solut() noexcept { return *this; }
		const VarEnc& variable() const noexcept { return m_var; }
		VarEnc& variable() noexcept { return m_var; }

		Real variableDistance(const SolBase &x, int id_pro) const override {
			return GET_PRO(id_pro).variableDistance(*this, x);
		}

		Real variableDistance(const VarBase &x, int id_pro) const override {
			return GET_PRO(id_pro).variableDistance(m_var, x);
		}

		virtual void reset() {
			SolBase::reset();
			m_time_evaluate = 0;
		}

	protected:
		VarEnc m_var;
		size_t m_time_evaluate;
	};
}

#endif // !OFEC_SOLUTION_H
