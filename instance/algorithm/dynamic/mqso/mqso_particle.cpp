#include "mqso_particle.h"
#include "../../../../core/problem/continuous/continuous.h"

namespace ofec {
    //MQSOParticle::MQSOParticle() :
    //    MQSOParticle(CONTINUOUS_CAST->objective_size(), CONTINUOUS_CAST->num_constraints(), CONTINUOUS_CAST->variable_size()) {}

    MQSOParticle::MQSOParticle(size_t num_obj, size_t num_con, size_t size_var) :
        Particle(num_obj, num_con, size_var),
        mv_repulse(size_var) {}

    MQSOParticle::~MQSOParticle() {

    }
    MQSOParticle::MQSOParticle(const MQSOParticle& other):Particle(other)
    {
        mv_repulse.resize(other.m_var.size());
        m_type = other.m_type;
        copy(other.mv_repulse.begin(), other.mv_repulse.end(), mv_repulse.begin());
    }

    void MQSOParticle::nextVelocity(const Solution<>* lbest, Real w, Real c1, Real c2, int id_rnd) {
        for (size_t j = 0; j < m_var.size(); j++) {
            if (m_type == ParticleType::PARTICLE_NEUTRAL) {
                m_vel[j] = w * m_vel[j] + c1 * GET_RND(id_rnd).uniform.next() * (m_pbest.variable()[j] - m_var[j]) + c2 * GET_RND(id_rnd).uniform.next() * (lbest->variable()[j] - m_var[j]);
            }
            else if (m_type == ParticleType::PARTICLE_CHARGED) {
                m_vel[j] = w * m_vel[j] + c1 * GET_RND(id_rnd).uniform.next() * (m_pbest.variable()[j] - m_var[j]) + c2 * GET_RND(id_rnd).uniform.next() * (lbest->variable()[j] - m_var[j]) + mv_repulse[j];
            }
            if (m_vel[j] > m_vMax[j].m_max)	m_vel[j] = m_vMax[j].m_max;
            else if (m_vel[j] < m_vMax[j].m_min)		m_vel[j] = m_vMax[j].m_min;
        }
    }
    void MQSOParticle::move() {
        if (m_type == ParticleType::PARTICLE_NEUTRAL || m_type == ParticleType::PARTICLE_CHARGED) {
            for (size_t j = 0; j < m_var.size(); j++) {
                m_var[j] += m_vel[j];
            }
        }
    }
    void MQSOParticle::quantumMove(const Solution<>* lbest, Real m_Rcloud, int id_rnd)
    {
        for (size_t j = 0; j < m_var.size(); j++) {
            Real temp = GET_RND(id_rnd).uniform.nextNonStd<Real>(-1, 1);
            m_var[j] = lbest->variable()[j] + m_Rcloud * temp;
        }
    }

    void MQSOParticle::initializeVmax(int id_pro) {
        for (int i = 0; i < GET_CONOP(id_pro).numVariables(); i++) {
            double l, u; 
            l = GET_CONOP(id_pro).range(i).first;
            u = GET_CONOP(id_pro).range(i).second;
            // for static optimization problems

            m_vMax.resize(GET_CONOP(id_pro).numVariables());
            if (GET_PRO(id_pro).name().find("FUN_") != std::string::npos)
                m_vMax[i].m_max = (u - l) / 2;
            else // for dynamic problems
                m_vMax[i].m_max = (u - l) / 5;
            m_vMax[i].m_min = -m_vMax[i].m_max;

        }
    }
    void MQSOParticle::initVelocity(int id_pro, int id_rnd) {
        for (size_t i = 0; i < m_var.size(); i++) {
            auto& range = GET_CONOP(id_pro).range(i);
            m_vel[i] = (range.second - range.first) * (-0.5 + GET_RND(id_rnd).uniform.next()) / 2;
        }
    }
}